#include <iostream>

double getDouble() {
    double num{};

    std::cout << "Enter a double value: ";
    std::cin >> num;

    return num;
}

char getOperator() {
    char oper{};

    std::cout << "Enter one of the following: +, -, *, or /: ";
    std::cin >> oper;

    return oper;
}

void printResult(const double num1, const double num2, const char oper) {
    std::cout << num1 << ' ' << oper << ' ' << num2 << " is ";

    switch (oper) {
        case '+':
            std::cout << num1 + num2;
            break;
        case '-':
            std::cout << num1 - num2;
            break;
        case '*':
            std::cout << num1 * num2;
            break;
        case '/':
            std::cout << num1 / num2;
            break;
        default:
            std::cout << "\nerror: Invalid operator inserted.\n";
            exit(1);
    }

    std::cout << ".\n";
}

int main() {
    double num1{getDouble()};
    double num2{getDouble()};
    char oper{getOperator()};

    printResult(num1, num2, oper);

    return 0;
}