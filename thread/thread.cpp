// g++ thread.cpp -pthread

#include <iostream>
#include <thread>

void thread_func(std::string message) {
    std::cout << message << '\n';
}

int main() {
    std::thread output_thread(thread_func, "test message");
    output_thread.join();
}
