#include <bitset>
#include <iostream>

std::bitset<8> rotl(std::bitset<8> bits, size_t rotate_by) {
    rotate_by %= 8;

	return (bits << rotate_by) | (bits >> (bits.size() - rotate_by));
}

std::bitset<8> rotr(std::bitset<8> bits, size_t rotate_by) {
    rotate_by %= 8;

	return (bits >> rotate_by) | (bits << (bits.size() - rotate_by));
}

int main()
{
	std::bitset<8> bits1{ 0b0000'0001 };
	std::cout << rotl(bits1, 2) << '\n';

	std::bitset<8> bits2{ 0b0000'1001 };
	std::cout << rotl(bits2, 7) << '\n';

	return 0;
}