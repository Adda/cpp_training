#include "constants.h"

#include <cmath>
#include <iostream>
#include <iomanip>

double getHeight() {
    double height{};
    std::cout << "Enter the height of the tower in meters: ";
    std::cin >> height;

    return height;
}

double calculateCurrentHeight(const double originalHeight, const int seconds) {
    double distanceFallen{constants::gravity * pow(seconds, 2) / 2.0};
    return originalHeight - distanceFallen;
}

bool printCurrentHeight(const double height, const int seconds) {
    std::cout << "At " << std::setw(3) << std::setfill(' ') << seconds << " seconds, the ball is ";

    bool onGround{false};
    if (height <= 0.) {
        std::cout << "on the ground.\n";
        onGround = true;
    } else {
        std::cout << "at height: " << std::setw(10) << std::fixed << std::setprecision(1) << height << " meters.\n";
    }

    return onGround;
}

int main() {
    const double originalHeight{getHeight()};

    int seconds{0};
    double height{originalHeight};

    bool onGround{false};
    while (!onGround) {
        height = calculateCurrentHeight(originalHeight, seconds);
        onGround = printCurrentHeight(height, seconds);
        seconds++;
    }

    return 0;
}